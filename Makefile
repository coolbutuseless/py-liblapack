example_leastsquares:
	cc example_leastsquares.c -o example_leastsquares -L/opt/local/lib -lopenblas

clean:
	rm *.o example_leastsquares
