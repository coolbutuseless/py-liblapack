Python CFFI interface to OpenBLAS-LAPACK
========================================
This is a small bit of code to get [OpenBLAS's](openblas.net) [LAPACK](netlib.org/lapack) accessible from python using [cffi](https://bitbucket.org/cffi/cffi).  It assumes you have already have a lapack library for your system and have it installed somewhere in your library search path.

The interface uses [The LAPACKE C Interface to LAPACK](http://www.netlib.org/lapack/lapacke.html). By default, it attempts to use the LAPACK that comes with OpenBLAS, but by changing the `dlopen` statement, you could use other compatible LAPACK libraries.

Files
=====
* `liblapack.py` is a 4-line file which is that totality of the python code to wrap LAPACK using CFFI. The rest of the work goes into preparing a header file suitable for parsing with CFFI.
    - `liblapack.h` contains both single- and double-precision function declarations
    - `liblapacks.h` contains just the single-precision function declarations
    - `liblapackd.h` contains just the double-precision function declarations

Example - LeastSquares (dgels)
==================================
The documentation for the C Interface to LAPACK includes some examples, including one on [calling 'dgels' to solve the least squares problems](http://www.netlib.org/lapack/lapacke.html#_calling_code_dgels_code).

* See `example_leastsquares.py` for a demonstration of:
    - using python `array.array`s to store data
    - using cffi to obtain C `double *` pointers to the array data
    - calling `liblapack.LAPACKE_dgels` to solve
* Original `C` function is included.
    - run `make` to create the `example_leastsquares` executable.
* Both the `C` version and the `Python` version call the same LAPACK function and should produce the same answer

Example - Solve (dgesv)
=======================
* Example code to solve `Ax=b` for `x` given a matrix `A` and vector `b`.
* See `example_solve.py` for a demonstration of:
    - using cffi to allocate memory
    - calling `liblapack.LAPACKE_dgesv` to solve


OSX OpenBLAS installation
=====================
* port install openblas +lapack
* `export DYLD_FALLBACK_LIBRARY_PATH="/opt/local/lib"`

Using other LAPACKs
===================
* Other LAPACK libraries with a C interface (i.e. LAPACKE) should be easy to use instead of the default OpenBLAS LAPACK.
* Simply change the `dlopen` statement in `liblapack.py` to point to your LAPACK library
* This has been tested with [the official LAPACK library](http://www.netlib.org/lapack/#_software) compiled from source.
* **OSX Note** The builtin LAPACK in OSX's Accelerate framework doesn't provide LAPACKE C interface. So it won't work here.

Notes
=====
* Code has only been tested with OSX Yosemite
* LAPACK library: macports version of OpenBLAS compiled with LAPACK enabled.
* Since CFFI does lots of great magic, most of the work involved goes into creating a header file which is acceptable to `cffi cdef`.
