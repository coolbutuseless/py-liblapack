#!/usr/bin/env python
from __future__ import print_function
import os
import sys
import re

accepted_types = ['', 's', 'd'] # singles and doubles only for now, as CFFI 0.8.6 isn't friendly to 'complex'
accepted_interface = 'LAPACKE_' # don't include the original "LAPACK_*" funcs, only "LAPACKE_*" ones

text = open("reference/lapacke.h").read()

for atype in accepted_types:
    actual = 0
    with open("build_header/liblapack_bottom.h", "w") as out:
        functions = re.findall("(?:lapack_int|float|double|void)\s+LAPACK.*?\(.*?\);", text, re.DOTALL)
        for function in functions:
            if not "LAPACKE_{type}".format(type=atype) in function:
                continue
            if "lapack_complex" in function:
                continue
            elif "_SELECT" in function:
                out.write("/* ToDo LAPACK_X_SELECT */\n/*" + function + "*/\n")
            else:
                actual += 1
                out.write(function + "\n")

    print(len(functions), "functions read from lapacke.h")
    outfile = "liblapack{type}.h".format(type=atype)
    os.system("cat build_header/liblapack_top.h build_header/liblapack_bottom.h > {outfile}".format(outfile=outfile))
    print(actual, "working functions written to {outfile}".format(outfile=outfile))
