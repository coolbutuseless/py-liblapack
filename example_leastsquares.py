#!/usr/bin/env python
from __future__ import print_function

from array import array
from liblapack import liblapack
from cffi import FFI
ffi = FFI()

# example from: http://www.netlib.org/lapack/lapacke.html
# In this example, we wish solve the least squares problem
# min_x || B - Ax || for two right-hand sides using the LAPACK routine DGELS.
# For input we will use the 5-by-3 matrix
#     ( 1  1  1 )
#     ( 2  3  4 )
# A = ( 3  5  2 )
#     ( 4  2  5 )
#     ( 5  4  3 )
#
# and the 5-by-2 matrix
#     ( -10 -3 )
#     (  12 14 )
# B = (  14 12 )
#     (  16 16 )
#     (  18 16 )
#
# Expected answer: 3x2 matrix
# ( 2 1 )
# ( 1 1 )
# ( 1 2 )
expected_answer = [2, 1,
                   1, 1,
                   1, 2]

def get_double_pointer_from_array(x):
    return ffi.cast("double *", x.buffer_info()[0])

a = array('d', [1, 1, 1,
                2, 3, 4,
                3, 5, 2,
                4, 2, 5,
                5, 4, 3])
m, n = a_shape = (5, 3)
apointer = get_double_pointer_from_array(a)

b = array('d', [-10, -3,
                 12, 14,
                 14, 12,
                 16, 16,
                 18, 16])
b_shape = (5, 2)
bpointer = get_double_pointer_from_array(b)


nrhs = 2
lda = 3
ldb = 2

# lapack_int LAPACKE_dgels( int matrix_order, char trans, lapack_int m,
#                           lapack_int n, lapack_int nrhs, double* a,
#                           lapack_int lda, double* b, lapack_int ldb );

info = liblapack.LAPACKE_dgels(liblapack.LAPACK_ROW_MAJOR, b"N", m, n, nrhs, apointer, lda, bpointer, ldb)
assert info==0
print("Input array 'b' is overwritten with the answer:")
for i in range(3):
    print(round(b[2*i], 6), round(b[2*i+1], 6))

print("Answer maches expected answer?:", all(abs(x-y)<1e-6 for x,y in zip(expected_answer, b)))
