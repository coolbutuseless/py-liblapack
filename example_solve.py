#!/usr/bin/env python
from __future__ import print_function

from array import array
from liblapack import liblapack
from cffi import FFI
ffi = FFI()

# Solve: Ax=b for x
# A = [1, 2
#      3, 4]
#
# b = [17
#      39]
#
# Answer should be: [5,
#                    6]

print("Solving Ax=b")

# Setup A using cffi allocated memory
A = ffi.new("double[]", 4)
for i in range(4):
    A[i] = i+1
print("Initial A:", [i for i in A])

# Setup b
b = ffi.new("double[]", 2)
b[0] = 17
b[1] = 39
print("Initial b:", [i for i in b])

# C interface to LAPACK function for solving Ax=b
# lapack_int LAPACKE_dgesv( int matrix_order, lapack_int n, lapack_int nrhs,
#                           double* a, lapack_int lda, lapack_int* ipiv,
#                           double* b, lapack_int ldb );

n = 2 # dimensions of square matrix: A
nrhs = 1  # b can be a collection of columns. here it is just 1 column
lda = 2
ldb = 1
ipiv = ffi.new("int[]", n) # Working space for pivot information

info = liblapack.LAPACKE_dgesv(liblapack.LAPACK_ROW_MAJOR, n, nrhs, A, lda, ipiv, b, ldb)
assert info == 0
print("A gets modified in dgesv:", [i for i in A])
print("Solution:", [round(i, 6) for i in b])
print("Solution correct:", all(abs(m-n)<1e-6 for m,n in zip(b, [5, 6])))

