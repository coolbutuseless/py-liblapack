#!/usr/bin/env python
from __future__ import print_function
import os
from cffi import FFI
ffi = FFI()

this_dir = os.path.dirname(os.path.abspath(__file__)) + "/"
# Just loading headers for the functions which operate on arrays of "double"
with open(this_dir + "liblapackd.h") as header_file:
    ffi.cdef(header_file.read())
liblapack = ffi.dlopen("libopenblas.dylib")
